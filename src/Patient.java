import java.util.ArrayList;
import java.util.Iterator;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Class Description</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class Patient {

	
	private final long pin;
	private final String forename;
	private final String dateOfBirth; 
	private final char gender; 
	
	private String name;
	private Address address;
	private ArrayList<PatientCase> cases = new ArrayList<PatientCase>();
	
	/**
	 * Constructs a Patient object on the base of different parameters.
	 * 
	 * @param aPIN
	 *          Patient Identification Number.
	 * @param aName
	 *          Name of the patient.
	 * @param aForename
	 *          Forename of the patient.
	 * @param aDateOfBirth
	 *          Date of birth of the patient (Format: dd.mm.yyyy).
	 * @param aGender
	 *          Gender of the patient.
	 */
	public Patient(long aPIN, String aName, String aForename, 
		String aDateOfBirth, char aGender)
	{
		pin = aPIN;
		name = aName;
		forename = aForename;
		dateOfBirth = aDateOfBirth;
		gender = aGender;
	}
	
	/**
	 * Defines a case (Format for anAdmDate: dd.mm.yyyy, aSize: cm, aWeight: kg).
	 * 
	 * @param aCaseID 
	 *            Case ID
	 * @param anAdmReason 
	 *            Reason of the Administration
	 * @param anAdmDate 
	 *            Date of the admission (Format: dd.mm.yyyy).
	 * @param aSize 
	 *            Size of the patient in cm (should be: > 0, reasonable > 140).
	 * @param aWeight 
	 *            Weight of the patient in kg (should be: > 0).
	 */
	public void defineCase(long aCaseID, String anAdmReason, String anAdmDate,
		     int aSize, int aWeight)
	{
		PatientCase aCase = new PatientCase(
				aCaseID, anAdmReason, anAdmDate, 
				aSize, aWeight, dateOfBirth, gender);
		
		cases.add(aCase); 
	}
	
	/**
	 * Returns the address of the patient.
	 * @return address of the patient. 
	 *  
	 */
	public String getAdrdress()
	{
		String result = "No Address specified.";
		if (address != null) {
			result = address.getAdress();
		}
		return result; 
	}
	
	/**
	 * Returns the data of all cases.
	 * @return the data of all cases.
	 */
	public String getAllCases()
	{
		String result = "No cases available.";
			
		if (cases.size() != 0) {
			result = ""; 
			for (PatientCase patCase : cases) {
				result += patCase.getCaseData() + "\n"; 
			}
		}
		return result;
	}
	
	/**
	 * Returns the data of a case.
	 * @param caseID
	 *          ID of the Case to return.
	 * @return the data of the specified case.
	 */
	public String getCase(int caseID)
	{
		String result = "No cases available."; 
		if (cases.size() != 0) {

			for (PatientCase patCase : cases) {
				
				if (patCase.getCaseID() == caseID) {
					result = patCase.getCaseData();
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Returns the date of the birth of the patient (Format: dd.mm.yyyy).
	 * @return the date of the birth of the patient (Format: dd.mm.yyyy).
	 */
	public String getDateOfBirth()
	{
		return dateOfBirth;
	}
	
	/**
	 * Returns the forename of the patient.
	 * @return the forename of the patient.
	 */
	public String getForename()
	{
		return forename;
	}
	
	/**
	 * Returns the gender of the patient.
	 * @return the gender of the patient.
	 */
	public char getGender()
	{
		return gender;
	}
	
	/**
	 * Returns the name of the patient.
	 * @return the name of the patient.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Returns the most important data of the patient 
	 * (PIN, name, forename, date of birth (Format: dd.mm.yyyy), 
	 * gender, postal code, city).
	 * @return the most important data of the patient.
	 */
	public String getPatientData()
	{
		return "PIN: " + pin + "\n" 
				+ "Name: " + name + "\n"
				+ "Forename: " + forename + "\n"
				+ "Date of birth: " + dateOfBirth + "\n"
				+ "Gender: " + gender + "\n"
				+ "Postal Code: " + address.getPostalCode() + "\n"
				+ "City: " + address.getCity() + "\n";
	}
	
	/**
	 * Returns the Patient Identification Number.
	 * @return the Patient Identification Number.
	 */
	public long getPIN()
	{
		return pin;
	}
	
	/**
	 * Sets the address of the patient.
	 * @param <b>aStreet:</b> 
	 *             street where address lives
	 * @param <b>aStreetNumer:</b> 
	 *            street number of the street
	 * @param <b>aPostalCode:</b> 
	 *            postal code for the city
	 * @param <b>aCity:</b> 
	 *            name of the city
	 */
	public void setAddress(String aStreet, String aStreetNumber, 
			String aPostalCode, String aCity)
	{
		address = new Address(name, 
				forename, aStreet, aStreetNumber, aPostalCode, aCity);
		
	}
	
	/**
	 * ets the date of the last menstrual period (Format aDate: dd.mm.yyyy).
	 * @param aCaseID
	 *          ID of the Case.
	 * @param aDate
	 *          date of the last menstrual period
	 */
	public void setDateOfLastMenstrualPeriod(int aCaseID, String aDate)
	{
		if (cases.size() != 0) {

			for (PatientCase patCase : cases) {
				
				if (patCase.getCaseID() == aCaseID) 
				{
					patCase.setDateOfLastMenstrualPeriod(aDate);
				}
			}
		}	
	}
	
	/**
	 * Sets the name of the patient.
	 * @param aName 
	 *          the name of the patient.
	 */
	public void setName(String aName)
	{
		name = aName; 
	}
}
